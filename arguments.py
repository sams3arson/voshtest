from typing import NamedTuple
from pathlib import Path
from shutil import which
import argparse


class Arguments(NamedTuple):
    solution_path: Path
    tests_path: Path
    interpreter: Path | None
    quiet: bool
    no_stdin: bool
    no_stdout: bool
    no_answer: bool


def parse_arguments() -> Arguments:
    parser = argparse.ArgumentParser(description="Script for verifying solutions to ARSO (ВсОШ, All-Russian Students Olympiad)")
    parser.add_argument("-s", "--solution", metavar="SOLUTION_PATH", help="path to the solution program", required=True)
    parser.add_argument("-t", "--tests", metavar="TESTS_FOLDER_PATH", help="path to the folder with tests to the solution", required=True)
    parser.add_argument("-i", "--interpreter", metavar="INTERPRETER_EXEC", help="interpreter executable (required for languages like Python)", default=None)
    parser.add_argument("-q", "--quiet", action="store_true", default=False, help="don't show stdin/stdout/right answer")
    parser.add_argument("--no-stdin", action="store_true", default=False, help="don't show stdin")
    parser.add_argument("--no-stdout", action="store_true", default=False, help="don't show stdout")
    parser.add_argument("--no-answer", action="store_true", default=False, help="don't show right answer")

    arguments = parser.parse_args()

    if arguments.interpreter is not None:
        if Path(arguments.interpreter).exists():
            interpreter = Path(arguments.interpreter)
        elif Path(str(which(arguments.interpreter))).exists():
            interpreter = Path(str(which(arguments.interpreter)))
        else:
            raise FileNotFoundError(arguments.interpreter)
    else:
        interpreter = None

    return Arguments(solution_path=Path(arguments.solution), tests_path=
            Path(arguments.tests), interpreter=interpreter, quiet=arguments.quiet, no_stdin=arguments.no_stdin,
            no_stdout=arguments.no_stdout, no_answer=arguments.no_answer)

