from testlib import Result

def format_results(results: list[Result], show_stdin: bool, show_stdout: bool,
        show_answer: bool):
    formatted_results = str()
    passed = {True: "Yes", False: "No"}

    for num, result in enumerate(results):
        formatted_results += f"Test: {result.name}\n"
        if show_stdin:
            formatted_results += f"stdin: {result.stdin.strip()}\n"
        if show_stdout:
            formatted_results += f"stout: {result.stdout.strip()}\n"
        if show_answer:
            formatted_results += f"Answer: {result.answer.strip()}\n"
        formatted_results += f"Time: {result.time}\nPassed: {passed[result.passed]}"

        if num + 1 < len(results):
            formatted_results += "\n\n"

    return formatted_results

