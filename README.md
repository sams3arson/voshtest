# voshtest
A Python script for verifying solutions to tasks from All-Russian Students Olympiad (ARSO, ВсОШ).

ARSO provides archives with solutions and tests to solutions for olympiad tasks from previous years. I'm too lazy to test my solutions manually, so I made a script for that.

To install it, clone the repo, `cd` into it, and run:
```bash
$ ln -sf $(pwd)/main.py /usr/local/bin/voshtest
```
Now you can access the script by running `voshtest` from anywhere.

Script usage should be self-explanatory with:
```bash
$ voshtest -h
```

Example:
```bash
$ voshtest -s 5.py -t archive-9-11/5/tests -i python3
```

> Note: for compiled languages like C/C++, you just have to pass executable of solution to `-s` argument. For interpreted languages like Python, you have to pass script name to `-s` argument and interpreter unqualified name that is on `PATH` (or full path, if you want) to `-i` (as shown above).