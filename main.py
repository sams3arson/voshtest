#!/usr/bin/env python3

from arguments import parse_arguments
from testlib import test_solution
from formatter import format_results


def main():
    args = parse_arguments()
    results = test_solution(solution_path=args.solution_path, tests_path=args.tests_path,
            interpreter=args.interpreter)
    print(format_results(results, show_stdin=not args.no_stdin, show_stdout=not args.no_stdout,
        show_answer=not args.no_answer))


if __name__ == "__main__":
    main()

