from subprocess import run
from pathlib import Path
from typing import NamedTuple
from shutil import which
from time import time


class Result(NamedTuple):
    name: str
    passed: bool
    time: float
    stdin: str
    stdout: str
    answer: str


class NotDir(Exception):
    pass


def test_solution(solution_path: Path, tests_path: Path, interpreter: Path | None) -> list[Result]:
    if not all((solution_path.exists(), tests_path.exists())):
        raise FileNotFoundError("provide valid paths")

    if not tests_path.is_dir():
        raise NotDir("provided tests path is not a folder")
 
    if interpreter is not None:
        executable = [interpreter.absolute(), solution_path.absolute()]
    else:
        executable = [str(solution_path.absolute())]

    results = list()
    tests = sorted([(str(test_path.absolute()), test_path.name) for test_path \
            in tests_path.iterdir() if test_path.name.isdigit()])

    for test, test_name in tests:
        with open(test, "rb") as f:
            test_stdin = f.read()

        with open(test + ".a", "rb") as f:
            test_answer = f.read().decode().strip()
        
        start_time = time()
        test_instance = run(executable, input=test_stdin, capture_output=True,
            shell=True)
        finish_time = time()

        test_stdout = test_instance.stdout.decode().strip()

        results.append(Result(name=test_name,
            passed=(test_stdout == test_answer),
            time=round(finish_time - start_time, 3),
            stdin=test_stdin.decode(),
            stdout=test_stdout,
            answer=test_answer))

    return results

